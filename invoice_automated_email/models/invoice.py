# -*- coding: utf-8 -*-

from datetime import date

from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    _inherit = 'account.move'

    @api.model
    def process_overdue_invoices(self):
        today = date.today()
        overdue_invoices = self.search([
            ('invoice_date_due', '<', today),
            ('is_move_sent', '=', False),
            ('state', '=', 'posted')  # Ensure only posted invoices are processed
        ])
        mail_template = self.env.ref('account.email_template_edi_invoice')
        # for invoice in overdue_invoices:
        #     if mail_template:
        #         mail_template.send_mail(invoice.id, force_send=True)
        #     invoice.is_move_sent = True
        # Iterate over each overdue invoice
        for invoice in overdue_invoices:
            try:
                if mail_template:
                    mail_template.send_mail(invoice.id, force_send=True)
                invoice.is_move_sent = True
            except Exception as e:
                # Log the exception if any occurs
                _logger.exception(f"Failed to process invoice {invoice.id}: {str(e)}")