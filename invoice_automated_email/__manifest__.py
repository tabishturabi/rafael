# -*- coding: utf-8 -*-

{
    'name': 'Invoice Automated Email',
    'version': '1.0',
    'category': 'Mailing',
    'summary': 'Mailing',
    'description': """Mailing""",
    'author': 'Developer',
    'website': '',
    'depends': ['base', 'mail', 'account'],
    'data': [
        # Data files
        'data/menu.xml',


        # Security files
        'security/security.xml',
        'security/ir.model.access.csv',

        # View files
        'views/invoice.xml',

    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'license': 'AGPL-3',
}